//
//  ViewController.swift
//  LabelPrintExample
//
//  Created by Dmitry Mozyrchuk on 25/10/2017.
//  Copyright © 2017 Dmitry Mozyrchuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var printButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func printButtonTapped(_ sender: Any) {
        let printController = UIPrintInteractionController.shared
        let printInfo = UIPrintInfo(dictionary: nil)
        printInfo.outputType = UIPrintInfoOutputType.photo
        printInfo.jobName = "print Job"
        printController.printInfo = printInfo

        let imageView = UIImageView(image: self.generateQRLabel())
        imageView.contentMode = UIViewContentMode.center
        UIGraphicsBeginImageContextWithOptions(CGSize(width: imageView.frame.size.width, height: 90 + 60 + 90), false, 0.0)

        imageView.image!.draw(in: CGRect(x: 2, y: 0, width: 70, height: 70))
        imageView.image!.draw(in: CGRect(x: 7, y: 120 , width: 60, height: 60))

        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
//        let newImageView = UIImageView(image: newImage)
        printController.printingItem = newImage
        printController.present(animated: true, completionHandler: nil)
    }

    func generateQRLabel() -> UIImage {
         let diceRoll = Int(arc4random_uniform(1000))
        let qrImage = UIImage.generateQRCodeImage("\(diceRoll)")
        return qrImage
    }

}

