//
//  UIImage+Extensions.swift
//  LabelPrintExample
//
//  Created by Dmitry Mozyrchuk on 25/10/2017.
//  Copyright © 2017 Dmitry Mozyrchuk. All rights reserved.
//

import UIKit
import CoreImage

extension UIImage {

    class func generateQRCodeImage(_ message: String) -> UIImage {
        let stringData = message.data(using: String.Encoding.isoLatin1)

        let filter = CIFilter(name: "CIQRCodeGenerator")

        filter?.setValue(stringData, forKey: "inputMessage")

        filter?.setValue("H", forKey: "inputCorrectionLevel")

        let scaleX = 100.0 / filter!.outputImage!.extent.size.width
        let scaleY = 100.0 / filter!.outputImage!.extent.size.height
        let transformedImage = filter!.outputImage!.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))

        let resultedImage = UIImage(ciImage: transformedImage)
        return resultedImage
    }

    func normalizedImage() -> UIImage {

        if self.imageOrientation == UIImageOrientation.up {
            return self
        }

        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height)
        self.draw(in: rect)

        let normalizedImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return normalizedImage
    }

    func resizeToSize(_ size: CGFloat) -> UIImage? {
        guard let imageData = UIImageJPEGRepresentation(self, 1.0)
            else {
                return nil
        }
        if CGFloat(imageData.count) > size {
            let divider = CGFloat(imageData.count) / size
            let newSize = CGSize(width: self.size.width / divider, height: self.size.height / divider)
            UIGraphicsBeginImageContextWithOptions(newSize, false, self.scale)
            self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
            let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
            return newImage
        } else {
            return self
        }
    }

}
